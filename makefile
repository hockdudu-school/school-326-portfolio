SHELL = /bin/sh

LATEX = lualatex

FILES = Journal.tex diary-2019-05-09.tex diary-2019-05-16.tex

all: Journal.pdf

Journal.pdf: $(FILES)
	-mkdir pdfbuild
	(cd pdfbuild && \
	TEXINPUTS=.:..: && \
	export TEXINPUTS && \
	$(LATEX) Journal.tex && $(LATEX) Journal.tex && \
	mv Journal.pdf .. )
	rm -r pdfbuild

clean:
	-rm -r pdfbuild


